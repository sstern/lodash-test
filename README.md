# lodash-test
## Purpose
To confirm that tree shaking occurs in `lodash` v4 with `webpack` v4 without using `lodash-es`.

## How to use
- yarn install
- npx webpack
  - youll want to run this for each of the 3 import statements I have provided in `index.js`
    - `import _ from 'lodash';`
    - `import { add } from 'lodash';`
    - `import add from 'lodash/add';`
- Compare each of the webpack bundle sizes and analyze the `dist/main.js` file

## Results
- import _ from 'lodash';
  - `main.js  72 KiB       0  [emitted]  main`
- import { add } from 'lodash';
  - `main.js  72 KiB       0  [emitted]  main`
- import add from 'lodash/add';
  - `main.js  2.92 KiB       0  [emitted]  main`

  ## Conclusion
  - Webpack combined with lodash v4 natievly will treeshake and only require the function being imported using the `import func from "lodash/func";` syntax.
  - Babel plugin lodash accomplishes the tree shaking capabilities while using the sntax (Cannot use _.chain when using this plugin)
   - `import _ from "lodash"`
   - `import { fun } from "lodash"`





